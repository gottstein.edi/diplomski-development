import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { CarAddInterfaceShort, CarAddInterfaceFull, CarAddCreateInterface } from '../model/carAdd.model';
import { Observable } from 'rxjs';
import { ImageInterface, ImageUploadClass, ImageUpload } from '../model/image.model';

@Injectable({
  providedIn: 'root'
})
export class AddsService {

  constructor(private _http:HttpClient) { }

  private readonly addsApi = `/api/adds`

  getAll(page: number, size: number):Observable<HttpResponse<CarAddInterfaceShort[]>>{
    page = page==undefined? 0: page;
    size = size==undefined? 5: size;

    return this._http.get<CarAddInterfaceShort[]>(`${this.addsApi}?page=${page}&size=${size}`,
    {observe: 'response'});
  }

  getAdd(addId: number):Observable<CarAddInterfaceFull>{
    return this._http.get<CarAddInterfaceFull>(`${this.addsApi}/${addId}`);
  }

  getUserAdds(userId: number):Observable<CarAddInterfaceShort[]>{
    return this._http.get<CarAddInterfaceShort[]>(`/api/users/${userId}/adds`);
  }
  
  getAddImages(addId: number):Observable<ImageUpload[]>{
    return this._http.get<ImageUpload[]>(`${this.addsApi}/${addId}/images`);
  }

  createAdd(add: CarAddCreateInterface):Observable<any>{
    return this._http.post(this.addsApi, add);
  }

  
  uploadImages(images: ImageUploadClass[], addId: number):Observable<any>{
    return this._http.post(`${this.addsApi}/${addId}/images`, images);
  }



  deleteAdd(addId: number):Observable<any>{
    return this._http.delete(`${this.addsApi}/${addId}`);
  }

}
