import { Component, OnInit } from '@angular/core';
import { AddsService } from './adds.service';
import { CarAddInterfaceShort } from '../model/carAdd.model';
import { AuthenticationService } from '../security/authentication.service';

@Component({
  selector: 'app-adds',
  templateUrl: './adds.component.html',
  styleUrls: ['./adds.component.css']
})
export class AddsComponent implements OnInit {

  adds: CarAddInterfaceShort[] = [];
  page: number = 1;
  size: number = 10;
  total: number;
  userRole: string;

  constructor(private _addsService: AddsService, private _authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.refreshAdds();
    this.userRole = this._authenticationService.roleLoggedIn();

  }

  refreshAdds(){
    this._addsService.getAll(this.page-1, this.size).subscribe(data => {
      this.total = Number(data.headers.get('X-Total'));
      this.adds  = data.body;
    }, error =>{
      this.adds = null;
      if(error.status == 401){
        localStorage.clear();
      }
    } );
  }

  onSelectChange() {
    this.refreshAdds();
  }

  onPageChange(event: number) {
    this.page = event;
    this.refreshAdds();
  }
  

  

}
