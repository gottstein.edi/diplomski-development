import { Component, OnInit } from '@angular/core';
import { MakeService } from './make.service';
import { CarMakeInterface } from '../model/make.model';
import { CarModelInterface } from '../model/carModel.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CarModelService } from '../car-model/car-model.service';

@Component({
  selector: 'app-makes',
  templateUrl: './makes.component.html',
  styleUrls: ['./makes.component.css']
})
export class MakesComponent implements OnInit {

  carMakes: CarMakeInterface[] = [];
  selectedMakeId: number;
  carModels: CarModelInterface[] = [];
  selectedModelId: number;
  selectedCarModel: CarModelInterface;
  makeAdd: CarMakeInterface = {
    name: null
  }
  carAdd: CarModelInterface = {
    name: null
  }

  constructor(private _carMakeService:MakeService, private modalService:NgbModal, private _modelService:CarModelService) { }

  ngOnInit() {
    this.refreshMakes();
  }

  onMakeClick(position: number){
    this.selectedMakeId = this.carMakes[position].id;
    this.refreshCars();
  }

  refreshCars(){
    this._carMakeService.getMakeCars(this.selectedMakeId).subscribe(data => {
      this.carModels = data;
    })
  }

  refreshMakes(){
    this._carMakeService.getAll().subscribe(data => {
      this.carMakes = data;
    })
  }

  onModelClick(position: number){
    this.selectedModelId = this.carModels[position].id;
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  deleteModelSelect(selectedModelPosition: number){
    this.selectedCarModel = this.carModels[selectedModelPosition];
    
  }

  deleteModel(){
    this._modelService.deleteOne(this.selectedCarModel.id).subscribe(data => {
      this.refreshCars();
    });
    
  }

  deleteSelectedMake(){
    this._carMakeService.deleteMake(this.selectedMakeId).subscribe(data => {
      this.refreshMakes();
      this.carModels = [];
      this.selectedMakeId = null;
    })
  }
  
  addNewMake(){
    this._carMakeService.post(this.makeAdd).subscribe(data=> {
      this.refreshMakes();
    })
  }

  addNewCar(){
    this._carMakeService.addCar(this.selectedMakeId, this.carAdd).subscribe(data => {
      this.refreshCars();
    })
  }

}
