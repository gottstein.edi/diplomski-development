import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CarMakeInterface } from '../model/make.model';
import { Observable } from 'rxjs';
import { CarModelInterface } from '../model/carModel.model';

@Injectable({
  providedIn: 'root'
})
export class MakeService {

  
  private readonly makesApi = `api/makes`

  constructor(private _http:HttpClient) { }

  getAll():Observable<CarMakeInterface[]>{
    return this._http.get<CarMakeInterface[]>(this.makesApi);
  }

  getMakeCars(makeId: number):Observable<CarModelInterface[]>{
    return this._http.get<CarModelInterface[]>(`${this.makesApi}/${makeId}/cars`);
  }

  deleteMake(makeId: number):Observable<any>{
    return this._http.delete(`${this.makesApi}/${makeId}`);
  }

  post(make: CarMakeInterface):Observable<any>{
    return this._http.post(this.makesApi, make);
  }

  addCar(makeId: number, carModel: CarModelInterface):Observable<any>{
    return this._http.post(`${this.makesApi}/${makeId}/cars`, carModel);
  }
}
