import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SecurityGuard } from "./security-guards/security-guard.guard";
import { AddsComponent } from "./adds/adds.component";
import { ProfileComponent } from "./profile/profile.component";
import { UsersComponent } from "./users/users.component";
import { MakesComponent } from "./makes/makes.component";
import { CitiesComponent } from "./cities/cities.component";
import { UserAddsComponent } from "./user-adds/user-adds.component";
import { RegistrationComponent } from "./registration/registration.component";
import { AddDetailsComponent } from "./add-details/add-details.component";
import { AddCreateComponent } from "./add-create/add-create.component";
import { AdminGuard } from "./security-guards/admin-guard.guard";
import { UnauthorizedComponent } from "./unauthorized/unauthorized.component";
import { UserDetailsComponent } from "./users/user-details/user-details.component";
import { SellerGuard } from "./security-guards/seller-guard.guard";

const appRoutes: Routes =[
    { path: '', component: AddsComponent, canActivate:[SecurityGuard]},
    { path: 'home', component: AddsComponent, canActivate:[SecurityGuard]},
    { path: 'profile', component: ProfileComponent, canActivate:[SecurityGuard]},
    { path: 'users', component: UsersComponent, canActivate:[SecurityGuard, AdminGuard]},
    { path: 'makes', component: MakesComponent, canActivate:[SecurityGuard, AdminGuard]},
    { path: 'adds/:id', component: AddDetailsComponent, canActivate:[SecurityGuard]},
    { path: 'cities', component: CitiesComponent, canActivate:[SecurityGuard, AdminGuard]},
    { path: 'user-adds', component: UserAddsComponent, canActivate:[SecurityGuard]},
    { path: 'registration', component:RegistrationComponent, canActivate:[SecurityGuard]},
    { path: 'add-create', component:AddCreateComponent, canActivate:[SecurityGuard, SellerGuard]},
    { path: 'unauthorized', component:UnauthorizedComponent, canActivate:[SecurityGuard]},
    { path: 'user-details/:id', component: UserDetailsComponent, canActivate:[SecurityGuard, AdminGuard]}


    

];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes,  {useHash: true})
    ],
    exports: [RouterModule],
    declarations: []
})
export class AppRoutingModule {}