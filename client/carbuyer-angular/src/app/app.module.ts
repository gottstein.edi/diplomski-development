import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './security/token-interceptor.service';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NavigationComponent } from './navigation/navigation.component';
import { AppRoutingModule } from './app-routing.module';
import { AddsComponent } from './adds/adds.component';
import { ProfileComponent } from './profile/profile.component';
import { FormsModule } from '@angular/forms';
import { MakesComponent } from './makes/makes.component';
import { UsersComponent } from './users/users.component';
import { CitiesComponent } from './cities/cities.component';
import { UserAddsComponent } from './user-adds/user-adds.component';
import { RegistrationComponent } from './registration/registration.component';
import { AddDetailsComponent } from './add-details/add-details.component';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { AddCreateComponent } from './add-create/add-create.component';
import { UserDetailsComponent } from './users/user-details/user-details.component';

@NgModule({
  declarations: [
    AppComponent,
    UnauthorizedComponent,
    NavigationComponent,
    AddsComponent,
    ProfileComponent,
    MakesComponent,
    UsersComponent,
    CitiesComponent,
    UserAddsComponent,
    RegistrationComponent,
    AddDetailsComponent,
    AddCreateComponent,
    UserDetailsComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
