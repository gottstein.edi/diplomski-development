import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CityInterface } from '../model/city.model';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  private readonly citiesApi = `api/cities`;


  constructor(private _http:HttpClient) { }

  getAll(name: string, page: number, size: number):Observable<HttpResponse<CityInterface[]>>{
    name = name==undefined? '':name;
    page = page==undefined? 0: page;
    size = size==undefined? 5: size;

    return this._http.get<CityInterface[]>(`${this.citiesApi}?name=${name}&page=${page}&size=${size}`,
    {observe: 'response'});
  }

  delete(cityId: number):Observable<any>{
    return this._http.delete(`${this.citiesApi}/${cityId}`);
  }

  addCity(city: CityInterface):Observable<any>{
    return this._http.post(this.citiesApi, city);
  }
}
