import { Component, OnInit } from '@angular/core';
import { CitiesService } from './cities.service';
import { CityInterface } from '../model/city.model';
import { filter } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  cities: CityInterface[] = [];

  page:number = 1;
  size: number = 10;
  total: number;
  filter: string = ""
  cityAdd: CityInterface = {
    name: null,
    zipCode: null
  }

  constructor(private _citiesService:CitiesService, private modalService: NgbModal) { }

  ngOnInit() {

    this.refreshData();
  }

  refreshData() {
    this._citiesService.getAll(this.filter, this.page-1, this.size).subscribe(data => {
      this.total = Number(data.headers.get('X-Total'));
      this.cities = data.body;
    }, error => this.cities = null);
  }

  onSearchChange(){
    this.refreshData();
  }

  onSelectChange(){
    this.refreshData();
  }

  onPageChange(event: number) {
    this.page = event;
    this.refreshData();
  }

  deleteCity(cityIndex: number){
    this._citiesService.delete(this.cities[cityIndex].id).subscribe(data => {
      this.refreshData();
    })
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  addCity(){
    this._citiesService.addCity(this.cityAdd).subscribe(data => {
      this.refreshData();
    })
  }





}
