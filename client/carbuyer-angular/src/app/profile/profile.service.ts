import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInterface } from '../model/user.model';
import { SellerInterface } from '../model/seller.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private readonly profileApi = `api/me`

  constructor(private _http:HttpClient) { }

  me():Observable<UserInterface | SellerInterface>{
    return this._http.get<UserInterface | SellerInterface>(this.profileApi);
  }
}
