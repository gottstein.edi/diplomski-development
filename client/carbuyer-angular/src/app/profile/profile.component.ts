import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import { UserInterface } from '../model/user.model';
import { SellerInterface } from '../model/seller.model';
import { Location } from '@angular/common';
import { PasswordChangeInterface } from '../model/passwordChange.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../users/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  loggedInUser: UserInterface | SellerInterface;
  passwordChange: PasswordChangeInterface = {
    newPassword: null,
    oldPassword: null
  }

  constructor(private _profileService: ProfileService, private _location:Location, private modalService: NgbModal, private _userService:UserService) { }

  ngOnInit() {
    this._profileService.me().subscribe(data => {
      this.loggedInUser = data;
    })
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  onBack(){
    this._location.back();
  }

  changeUserPassword(){
    this._userService.changeUserPassword(this.passwordChange).subscribe(data => {
    }, error => {
    })
  }

}
