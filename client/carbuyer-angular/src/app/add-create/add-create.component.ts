import { Component, OnInit } from '@angular/core';
import { CarModelInterface } from '../model/carModel.model';
import { CarMakeInterface } from '../model/make.model';
import { MakeService } from '../makes/make.service';
import { CarModelService } from '../car-model/car-model.service';
import { CitiesService } from '../cities/cities.service';
import { CityInterface } from '../model/city.model';
import { AuthenticationService } from '../security/authentication.service';
import { CarAddCreateInterface } from '../model/carAdd.model';
import { AddsService } from '../adds/adds.service';
import { Router } from '@angular/router';
import { ImageUploadClass } from '../model/image.model';
import { FileUpload } from '../file-upload/fileupload';
import { FileUploadService } from '../file-upload/file-upload.service';


@Component({
  selector: 'app-add-create',
  templateUrl: './add-create.component.html',
  styleUrls: ['./add-create.component.css']
})
export class AddCreateComponent implements OnInit {

  carModels: CarModelInterface[] = [];
  carMakes: CarMakeInterface[] = [];
  cities: CityInterface[] = [];
  sellerId: number;
  years: number[] = Array.from({ length: (2020 - 1950) }, (v, k) => k + 1950).reverse();

  carAddCreate: CarAddCreateInterface = {
    city: null,
    details: null,
    make: null,
    mileage: null,
    model: null,
    price: null,
    seller: null,
    year: null
  }



  //IMAGE UPLOAD
  imagesForUpload: ImageUploadClass[] = [];

  selectedFiles: FileList;
  currentFileUpload: FileUpload;
  progress: { percentage: number } = { percentage: 0 };


  constructor(private _makeSerivce: MakeService, private _citiesService: CitiesService, private _authenticationService: AuthenticationService,
    private _addService: AddsService, private router: Router, private _fileUploadService: FileUploadService) { 
      
    }

  ngOnInit() {

    this.carAddCreate.seller = this._authenticationService.loggedInUserId();
    this.loadCities();
    this.loadMakes();
  }

  loadCities() {
    this._citiesService.getAll("", 0, 200).subscribe(data => {
      this.cities = data.body;
    })
  }

  loadMakes() {
    this._makeSerivce.getAll().subscribe(data => {
      this.carMakes = data;
    })
  }

  refreshCarModels(makeId: number) {
    this._makeSerivce.getMakeCars(makeId).subscribe(data => {
      this.carModels = data;
    })
  }

  onModelSelectChange() {
    this.refreshCarModels(this.carAddCreate.make);
  }

  createAdd() {
    this._addService.createAdd(this.carAddCreate).subscribe(data => {
      if(this.imagesForUpload.length > 0){
        for(let image of this.imagesForUpload){
          image.addId = data.id;
        }
        this._addService.uploadImages(this.imagesForUpload, data.id).subscribe(data => {
          
        })
      }
      this.router.navigate(['/adds',data.id]);
      
    })
  }

  onCancel() {
    for(let image of this.imagesForUpload){
      this._fileUploadService.deleteFileUpload(image);
    }
    this.router.navigate(['home']);
  }




  //UPLOAD IMAGES
  selectFile(event) {
    const file = event.target.files.item(0);
    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
      this.uploadToFirebase();
    } else {
      alert('invalid format!');
    }
  }

  uploadToFirebase(){
    const file = this.selectedFiles.item(0);
    this.selectedFiles = undefined;
    this.currentFileUpload = new FileUpload(file);
    this._fileUploadService.pushFileToStorage(this.currentFileUpload, this.progress, (o)=> {
      let image = new ImageUploadClass();
      image.name = o.name;
      image.imageKey = o.imageKey;
      image.url = o.url;
      this.imagesForUpload.push(image);
      this.currentFileUpload = undefined;
    });
  }

  deleteFromFirebase(index: number){
    this._fileUploadService.deleteFileUpload(this.imagesForUpload[index], (o)=> {
      if(o){
        this.imagesForUpload.splice(index,1);
      }
    })
  }



}
