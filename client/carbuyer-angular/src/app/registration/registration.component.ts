import { Component, OnInit } from '@angular/core';
import { UserInterface } from '../model/user.model';
import { RegisteringUser } from '../model/user-register.model';
import { SellerRegisterInterface } from '../model/seller.model';
import { UserService } from '../users/user.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../security/authentication.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registeringUser: SellerRegisterInterface = {
    firstName: null,
    lastName: null,
    username: null,
    address: null,
    email: null,
    phoneNumber: null,
    password: null
  }

  userNameAvailable: boolean;

  constructor(private _userService:UserService, private _router:Router, private _authenticationService:AuthenticationService) { }

  ngOnInit() {
  }

  onRegister(){
    this._userService.register(this.registeringUser).subscribe(data => {
      this._router.navigate(['/home']);
    })
  }

  onUsernameChange(){
    this.userNameAvailable = undefined;
  }

  onUsernameCheck(){
    this._userService.checkUsernameAvailability(this.registeringUser.username).subscribe(data => {
      this.userNameAvailable = data;
    })
  }

}
