import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthenticationService } from "../security/authentication.service";
import { UserLoginInterface } from "../security/user.login.interface";
import { Observable } from "rxjs";


@Injectable({
    providedIn: 'root'
  })
export class SecurityGuard implements CanActivate {

    constructor(private _authenticationService: AuthenticationService){}

    userVisitor: UserLoginInterface = {
        password: 'visitor',
        username: 'visitor'
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if(this._authenticationService.isLoggedIn()){
          return true;
        }
        else{
          this._authenticationService.login(this.userVisitor).subscribe(data => {
            return true;
          });
          return false;
        }
      }

}