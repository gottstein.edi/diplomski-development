import { Injectable } from "@angular/core";
import { AuthenticationService } from "../security/authentication.service";
import { NavElement } from "./NavElement.model";

@Injectable({
    providedIn: 'root'
  })
  export class NavigationService{
      
    constructor(private _authenticationService:AuthenticationService) {}

    userNotVisitor():boolean {
        if(this._authenticationService.isVisitor()){
            return false;
        }
        return true;
    }

    navigationSetup():NavElement[]{
        if(this._authenticationService.isAdmin()){
            return [{name:"Home",path: "home"},{name: "Users",path: "users"},{name:"Makes", path: "makes"},{name:"Cities", path:"cities"}];
        }
        if(this._authenticationService.isSeller()){
            return [{name:"Home",path: "home"},{name: "My adds", path:"user-adds"}];
        } else{
            return [{name:"Home", path: "home"}];
        }
    }

    

    
  }