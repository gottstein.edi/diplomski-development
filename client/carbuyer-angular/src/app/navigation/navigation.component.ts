import { Component, OnInit } from '@angular/core';
import { UserLoginInterface } from '../security/user.login.interface';
import { NavElement } from './NavElement.model';
import { AuthenticationService } from '../security/authentication.service';
import { NavigationService } from './navigation.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  loggedInUserRole:string;
  loginError = false;
  modalReference:NgbModalRef;

  loggingUser: UserLoginInterface = {
    username: null,
    password: null
  }

  navElements:NavElement[] = [];

  isLoggedIn:boolean = false;

  constructor(private _authService:AuthenticationService, private _navigationService:NavigationService, private modalService: NgbModal, private _router:Router) { }

  ngOnInit() {

    this.isLoggedIn = !this._authService.isVisitor();

    if(this.isLoggedIn){
      this.loggedInUserRole = this._authService.roleLoggedIn();
    }

    this.navElements = this._navigationService.navigationSetup();

  }

  logIn(){
    this._authService.login(this.loggingUser).subscribe(data => {

      if(data){
        this.isLoggedIn = true;
        this.navElements = this._navigationService.navigationSetup();
        this._router.navigate(['/home']);
        this.loginError = false;
        this.modalReference.close();
      }
    }, error => {
      this.loginError = true;
    });
  }

  

  logOut(){
    localStorage.clear();
    this.isLoggedIn = false;
    this._authService.login({username: "visitor",password: "visitor"}).subscribe(data => {
      this.navElements = this._navigationService.navigationSetup();
      this._router.navigate(['/home']);
    })
    
  }

  open(content) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then((result) => {
    }, (reason) => {
    });
    }
  
  

}
