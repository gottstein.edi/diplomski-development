import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserLoginInterface } from './user.login.interface';
import { Observable } from 'rxjs';
import { UserToken } from './user.token';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly loginPath = `/auth/login`

  constructor(private _http:HttpClient) { }

  login(user: UserLoginInterface): Observable<boolean> {
    const body = `username=${user.username}&password=${user.password}`;
const httpOptions = {
  headers: new HttpHeaders(
    { 'Content-Type': 'application/x-www-form-urlencoded' })
        };
    return this._http.post<UserToken>(this.loginPath, body, httpOptions)
    .pipe(map((res: UserToken) => {
        let token = res.access_token;
        if (token) {
          localStorage.setItem('token', JSON.stringify({
                                    username: user.username,
                                    access_token: token,
                                    role: res.role,
                                    id: res.id
                                  }));

          return true;
        }
        else {
          return false;
        }
      }))
      
}

  getToken(): String {
    var token = JSON.parse(localStorage.getItem('token'));
    var access_token = token && token.access_token;
    return access_token ? access_token : "";
  }

  isLoggedIn(): boolean {
    if(this.getToken()!='') return true;
    else return false;
  }

  roleLoggedIn(): string {
    var token = JSON.parse(localStorage.getItem("token"));
    return token.role;
  }

  loggedInUserId():number {
    var token = JSON.parse(localStorage.getItem("token"));
    return token.id;
  }

  isVisitor():boolean{
    if(this.getToken()!=''){
      const token = JSON.parse(localStorage.getItem("token"));
      return token.role === "VISITOR";
    }
    return false;
    
  }

  isAdmin():boolean {
    if(this.getToken()!=''){
      const token = JSON.parse(localStorage.getItem("token"));
      return token.role === "ADMIN";
    }
    return false;
  }

  isSeller():boolean {
    if(this.getToken()!=''){
      const token = JSON.parse(localStorage.getItem("token"));
      return token.role === "SELLER";
    }
    return false;
  }



}
