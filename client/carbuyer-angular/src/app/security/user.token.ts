export interface UserToken{
    access_token: string;
    expires_in: number;
    role: string;
    id?: number;
}