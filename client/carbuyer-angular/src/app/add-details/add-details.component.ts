import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { CarAddInterfaceFull, CarAddCreateInterface } from '../model/carAdd.model';
import { AddsService } from '../adds/adds.service';
import { ImageInterface, ImageUpload } from '../model/image.model';
import { Location } from '@angular/common';
import { UserService } from '../users/user.service';
import { SellerInterface } from '../model/seller.model';
import { AuthenticationService } from '../security/authentication.service';
import { FileUploadService } from '../file-upload/file-upload.service';

@Component({
  selector: 'app-add-details',
  templateUrl: './add-details.component.html',
  styleUrls: ['./add-details.component.css']
})
export class AddDetailsComponent implements OnInit {
  images: ImageUpload[] = [];
  addId: number;
  add: CarAddInterfaceFull;
  seller: SellerInterface;
  userId: number;
  addEdit: CarAddCreateInterface = null;
  isAdmin: boolean = false;

  constructor(config: NgbCarouselConfig, private _router: Router, private route: ActivatedRoute, private _addService: AddsService, private _location: Location,
    private _userService: UserService, private _authenticationService: AuthenticationService, private modalService: NgbModal, private _fileService:FileUploadService) {
    config.interval = 0;
  }

  ngOnInit() {
    this.addId = +this.route.snapshot.paramMap.get("id");
    this._addService.getAdd(this.addId).subscribe(data => {
      this.add = data;
      this._userService.getSeller(this.add.seller).subscribe(data => {
        this.seller = data;
      })
      this._addService.getAddImages(this.addId).subscribe(data => {
        this.images = data;
      })
    })
    this.isAdmin = this._authenticationService.isAdmin();
    this.userId = this._authenticationService.loggedInUserId();
  }


  open(content) {
    this.modalService.open(content).result.then((result) => {

    }, (reason) => {

    });
  }

  deleteSelectedAdd(){
    this._addService.deleteAdd(this.addId).subscribe(data => {
      for (let i of this.images){
        this._fileService.deleteFileUpload(i);
      }
      this._router.navigate(['/home']);
    })
  }

  onBack() {
    this._location.back();
  }

}
