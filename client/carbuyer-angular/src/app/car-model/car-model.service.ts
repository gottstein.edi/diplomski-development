import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CarModelInterface } from '../model/carModel.model';

@Injectable({
  providedIn: 'root'
})
export class CarModelService {

  private readonly modelApi = `/api/cars`

  constructor(private _http:HttpClient) { }

  deleteOne(modelId: number):Observable<any>{
    return this._http.delete(`${this.modelApi}/${modelId}`);
  }

  
}
