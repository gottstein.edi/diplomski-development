import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { FileUpload } from './fileupload';

import * as firebase from 'firebase/app';
import 'firebase/storage';
import { ImageUpload } from '../model/image.model';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private imageUpload: ImageUpload = {
    addId: null,
    url: null,
    imageKey: null,
    name: null
  }

  private basePath = '/uploads';

  constructor(private db: AngularFireDatabase) { }

  pushFileToStorage(fileUpload: FileUpload, progress: { percentage: number }, callback?) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        // in progress
        const snap = snapshot as firebase.storage.UploadTaskSnapshot;
        progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100);
      },
      (error) => {
        // fail
        console.log(error);
      },
      () => {
        // success
        uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
          fileUpload.url = downloadURL;
          fileUpload.name = fileUpload.file.name;
          this.saveFileData(fileUpload);
          if (callback) {
            this.imageUpload.url = fileUpload.url;
            this.imageUpload.name = fileUpload.file.name;
            callback(this.imageUpload);
          }
        });
      }
    );
  }

  private saveFileData(fileUpload: FileUpload) {
    var newRef = this.db.list(`${this.basePath}/`).push(fileUpload);
    this.imageUpload.imageKey = newRef.key;
  }

  getFileUploads(numberItems): AngularFireList<FileUpload> {
    return this.db.list(this.basePath, ref =>
      ref.limitToLast(numberItems));
  }

  deleteFileUpload(image: ImageUpload, callback?) {
    this.deleteFileDatabase(image.imageKey)
      .then(() => {
        this.deleteFileStorage(image.name);
        if(callback){
          callback(true);
        }
      })
      .catch(error => console.log(error));
  }

  private deleteFileDatabase(key: string) {
    return this.db.list(`${this.basePath}/`).remove(key);
  }

  private deleteFileStorage(name: string) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete();
  }
}
