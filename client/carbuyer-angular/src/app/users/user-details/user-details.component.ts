import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { SellerInterface } from 'src/app/model/seller.model';
import { UserInterface } from 'src/app/model/user.model';
import { AddsService } from 'src/app/adds/adds.service';
import { CarAddInterfaceShort } from 'src/app/model/carAdd.model';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  userId: number;
  user: SellerInterface|UserInterface;
  userAdds: CarAddInterfaceShort[] = [];

  constructor(private _location:Location, private modalService: NgbModal, private route: ActivatedRoute, private _userService:UserService, private _addService:AddsService) { }

  ngOnInit() {
    this.userId = +this.route.snapshot.paramMap.get("id");
    this._userService.getUser(this.userId).subscribe(data => {
      this.user = data;
    })
  }


  onBack(){
    this._location.back();
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  tabChanged(event: any){
    if(event.nextId == "userAdds"){
      this._addService.getUserAdds(this.userId).subscribe(data => {
        this.userAdds = data;
      });
    }
    
  }

  deleteSelectedUser(){
    this._userService.deleteUser(this.userId).subscribe(data => {
      this._location.back();
    })
  }


}
