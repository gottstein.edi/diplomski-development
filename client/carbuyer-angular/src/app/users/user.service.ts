import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInterface } from '../model/user.model';
import { SellerInterface, SellerRegisterInterface } from '../model/seller.model';
import { PasswordChangeInterface } from '../model/passwordChange.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly usersApi = `api/users`;
  private readonly registerApi = `api/register`;
  private readonly passwordChangeApi = `api/me/change-password`;

  constructor(private _http: HttpClient) { }

  getAll(username: string, page: number, size: number):Observable<HttpResponse<UserInterface[]>>{
    username = username==undefined? '':username;
    page = page==undefined? 0: page;
    size = size==undefined? 5: size;
    return this._http.get<UserInterface[]>(`${this.usersApi}?username=${username}&page=${page}&size=${size}`, 
    {observe: 'response'});
  }

  getSeller(sellerId:number):Observable<SellerInterface>{
    return this._http.get<SellerInterface>(`${this.usersApi}/${sellerId}`);
  }

  register(seller:SellerRegisterInterface):Observable<any>{
    return this._http.post(`${this.registerApi}`, seller);
  }

  getUser(userId: number):Observable<SellerInterface|UserInterface>{
    return this._http.get<SellerInterface|UserInterface>(`${this.usersApi}/${userId}`);
  }


  checkUsernameAvailability(username: string):Observable<boolean>{
    return this._http.get<boolean>(`${this.usersApi}/username-check/${username}`);
  }


  changeUserPassword(passwordChange:PasswordChangeInterface):Observable<any>{
    return this._http.put(`${this.passwordChangeApi}`, passwordChange);
  }

  deleteUser(userId: number):Observable<any>{
    return this._http.delete(`${this.usersApi}/${userId}`);
  }

  
}
