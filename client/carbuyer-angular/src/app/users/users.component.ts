import { Component, OnInit } from '@angular/core';
import { UserInterface } from '../model/user.model';
import { UserService } from './user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: UserInterface[] = [];

  page:number = 1;
  size: number = 5;
  total: number;
  filter: string = ""

  constructor(private _userService: UserService) { }

  ngOnInit() {
    this.refreshData();
  }

  refreshData() {
    this._userService.getAll(this.filter, this.page-1, this.size).subscribe(data => {
      this.total = Number(data.headers.get('X-Total'));
      this.users = data.body;
    }, error => this.users = null);
  }

  onSearchChange(){
    this.refreshData();
  }

  onSelectChange(){
    this.refreshData();
  }

  onPageChange(event: number){
    this.page = event;
    this.refreshData();
  }

}
