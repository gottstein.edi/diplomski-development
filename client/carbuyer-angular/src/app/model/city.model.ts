export interface CityInterface {
    id?: number,
    name: string,
    zipCode: string
}