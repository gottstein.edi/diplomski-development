export interface PasswordChangeInterface {
    oldPassword: string,
    newPassword: string
}