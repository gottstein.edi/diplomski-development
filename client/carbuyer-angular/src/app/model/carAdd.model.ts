export interface CarAddInterfaceShort {
    id: number,
    year: number,
    make: string,
    model: string,
    price: string,
    firstImage: string,
    details?: string,
}

export interface CarAddInterfaceFull {
    id: number,
    details: string,
    city: string,
    price: number,
    dateCreated: Date,
    mileage: number,
    year: number,
    seller: number,
    make: string,
    model: string
}

export interface CarAddCreateInterface {
    details: string,
    city: number,
    price: number,
    mileage: number,
    year: number,
    seller: number,
    make: number,
    model: number
}

