export interface SellerInterface {
    id?: number,
    firstName: string,
    lastName: string,
    username: string,
    address: string,
    phoneNumber: string,
    email: string,
    role: string
}

export interface SellerRegisterInterface {
    firstName: string,
    lastName: string,
    username: string,
    address: string,
    phoneNumber: string,
    email: string,
    password: string
}