export interface UserInterface {
    id?: number,
    firstName: string,
    lastName: string,
    username: string,
    role: string
}