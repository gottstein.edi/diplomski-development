export interface CarMakeInterface {
    id?: number,
    name: string
}