export interface RegisteringUser {
    firstName: string,
    lastName: string,
    username: string,
    password: string
}