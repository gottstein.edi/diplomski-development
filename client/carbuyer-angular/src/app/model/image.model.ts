export interface ImageInterface {
    url: string;
}

export interface ImageUpload {
    addId: number,
    url: string,
    name: string,
    imageKey: string
}

export class ImageUploadClass { 
    addId: number;
    url: string;
    name: string;
    imageKey: string;
}