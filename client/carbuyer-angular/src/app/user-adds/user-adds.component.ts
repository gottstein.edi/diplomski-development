import { Component, OnInit } from '@angular/core';
import { AddsService } from '../adds/adds.service';
import { AuthenticationService } from '../security/authentication.service';
import { CarAddInterfaceShort } from '../model/carAdd.model';

@Component({
  selector: 'app-user-adds',
  templateUrl: './user-adds.component.html',
  styleUrls: ['./user-adds.component.css']
})
export class UserAddsComponent implements OnInit {

  userAdds: CarAddInterfaceShort[] = [];

  constructor(private _addService: AddsService, private _authenticationService: AuthenticationService) { }



  ngOnInit() {
    this._addService.getUserAdds(this._authenticationService.loggedInUserId()).subscribe(data => {
      this.userAdds = data;
    })
  }


  onAddClick(){

  }

}
