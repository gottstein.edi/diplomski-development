package edmond.sf64.CarbuyerBackend;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import edmond.sf64.CarbuyerBackend.Config.WebSecurityConfiguration;
import edmond.sf64.CarbuyerBackend.Controllers.AddControler;
import edmond.sf64.CarbuyerBackend.Dtos.CarAddCreateDto;
import edmond.sf64.CarbuyerBackend.Entities.CarAdd;
import edmond.sf64.CarbuyerBackend.Entities.CarModel;
import edmond.sf64.CarbuyerBackend.Entities.City;
import edmond.sf64.CarbuyerBackend.Entities.Seller;
import edmond.sf64.CarbuyerBackend.Repositories.CarAddRepository;
import edmond.sf64.CarbuyerBackend.Repositories.UserRepository;
import edmond.sf64.CarbuyerBackend.Services.impl.CarAddService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import java.io.IOException;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestDataConfig.class, CarbuyerBackendApplication.class})
@WebAppConfiguration
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource(locations="classpath:application-test.properties")
@WithMockUser(username = "ram", roles={"SELLER"})
public class CarbuyerBackendApplicationTests {



	private MockMvc mockMvc;

	@Autowired
	UserRepository userRepository;

	@Autowired
	CarAddRepository carAddRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;


	@Autowired
	private HttpMessageConverter<?>[] httpMessageConverters;


	@Before
	public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	    MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() throws Exception {


		CarAddCreateDto dto = new CarAddCreateDto();
		dto.setCity(1);
		dto.setMake(1);
		dto.setSeller(1);
		dto.setModel(1);

		dto.setDetails("Great car, no problem so far");
		dto.setYear(2013);
		dto.setMileage(80000);
		dto.setPrice(2050);

		mockMvc.perform(
				post("/api/adds")
						.contentType(MediaType.APPLICATION_JSON)
						.content(convertObjectToJsonBytes(dto)))
				.andExpect(status().isOk());
	}

	@Test
	public void insertWithInvalidSellerId() throws Exception {

		CarAddCreateDto dto = new CarAddCreateDto();
		dto.setCity(1);
		dto.setMake(1);
		dto.setSeller(1);
		dto.setModel(1);

		dto.setDetails("details");
		dto.setYear(1700);
		dto.setMileage(-999);
		dto.setPrice(-9999);



		mockMvc.perform(
				post("/api/adds")
						.contentType(MediaType.APPLICATION_JSON)
						.content(convertObjectToJsonBytes(dto)))

				.andExpect(status().isBadRequest());


	}


	private static final ObjectMapper mapper = createObjectMapper();

	private static ObjectMapper createObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.registerModule(new JavaTimeModule());
		return mapper;
	}

	/**
	 * Convert an object to JSON byte array.
	 *
	 * @param object
	 *            the object to convert
	 * @return the JSON byte array
	 * @throws IOException
	 */
	public static byte[] convertObjectToJsonBytes(Object object)
			throws IOException {
		return mapper.writeValueAsBytes(object);
	}


}
