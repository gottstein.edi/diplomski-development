--ROLE
INSERT INTO role(name) VALUES('ROLE_ADMIN')
INSERT INTO role(name) VALUES('ROLE_SELLER')
INSERT INTO role(name) VALUES('ROLE_VISITOR')

--CITIESd

INSERT INTO city(name,zip_code,deleted) VALUES ('Novi Sad', '21000',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Belgrade', '423423',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Los Angeles', '353242',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('New York', '12312312',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Tihuana', '42421',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Mexico City', '12412412',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Berlin', '123123',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Dortmund', '124124',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Madrid', '12312312',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Paris', '124124124',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Amsterdam', '123123',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Geneva', '12421412',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Zurich', '41142421',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Wien', '123123123',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Zagreb', '12312312',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Linz', '1231231',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Graz', '123123123',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('Monaco', '123123123',0)
INSERT INTO city(name,zip_code,deleted) VALUES ('London', '123123123',0)

--USERS
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted) VALUES ('Admin','Edmond','Gottstein','admin','$2a$10$.aBG1VEsE3dd0OfU/zef5uCCmq.Gu3zLrk0x/iWZq2Yy76SrXnB7O',1,0)
--USERNAME: admin, PASSWORD: admin

--SELLER
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera1','Peric','seller','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera2','Peric','seller2','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera3','Peric','seller3ndf','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera4','Peric','seller4bnb','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera5','Peric','seller4cxcx','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera6','Peric','seller4zxz','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera7','Peric','seller4xxx','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera8','Peric','seller4ccc','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera9','Peric','seller42123c','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera10','Peric','seller4ggh','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera11','Peric','seller485g','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera12','Peric','seller4990','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera13','Peric','seller4896','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera14','Peric','seller4457','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera15','Peric','seller475','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera16','Peric','seller4777','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera17','Peric','seller4666','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera18','Peric','seller4555','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera19','Peric','seller41123','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera20','Peric','seller41','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted,email,address,phone_number) VALUES ('Seller','Pera21','Peric','seller42','$2a$10$0JuEhXtZVWAHcnxwp3tiouGAf/4fQo6FG/53/csbtvLZJtbdGOYYq',2,0,'gottstein.edi@gmail.com','Sarajevska 8', '065-3366634')

--VISITOR
INSERT INTO user(dtype,first_name,last_name,username,password,role_id,deleted) VALUES ('Visitor','Visitorin','Visitorovic','visitor','$2a$10$44kC6NYoKo9EzY3rz7UF0ucttnVjgstNKFIgRok.RLD60tZua6CWi',3,0)

--CAR_MAKES
INSERT INTO car_make(deleted,name) VALUES (0,'Ford')
INSERT INTO car_make(deleted,name) VALUES (0,'Audi')
INSERT INTO car_make(deleted,name) VALUES (0,'Kia')
INSERT INTO car_make(deleted,name) VALUES (0,'BMW')
INSERT INTO car_make(deleted,name) VALUES (0,'Mercedes-Benz')
INSERT INTO car_make(deleted,name) VALUES (0,'Porsche')
INSERT INTO car_make(deleted,name) VALUES (0,'Alfa-Romeo')
INSERT INTO car_make(deleted,name) VALUES (0,'Chevrolet')
INSERT INTO car_make(deleted,name) VALUES (0,'Fiat')
INSERT INTO car_make(deleted,name) VALUES (0,'Chrysler')

--CAR_MODELS
INSERT INTO car_model(name,make_id,deleted) VALUES ('Mustang',1,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Escort',1,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Focus',1,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('A5',2,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Q7',2,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('TT',2,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('Sportage',3,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Sorento',3,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Ceed',3,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('Serie 1',4,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Serie 3',4,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('X3',4,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('GLE',5,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('E-class',5,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('C-class',5,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('Cayenne',6,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Boxter',6,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('918',6,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('Giulia',7,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('159',7,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Stelvio',7,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('Malibu',8,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('HHR',8,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Camaro',8,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('Bravo',9,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Punto',9,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Stilo',9,0)

INSERT INTO car_model(name,make_id,deleted) VALUES ('300c',10,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Crossfire',10,0)
INSERT INTO car_model(name,make_id,deleted) VALUES ('Pacific',10,0)


INSERT INTO car_add(date_created, details, mileage, price, year, car_id, seller_id, city_id, deleted) VALUES ('2018-11-01', 'Automobil odlicno ocuvan. Prvi vlasnik, redovno odrzavan u ovlascenom servisu. Uz auto se dobijaju i 4 celicne felne sa zimskim gumama. Registrovan do kraja godine.', 150000, 2050, 2004, 1, 2, 3 ,0)
INSERT INTO car_add(date_created, details, mileage, price, year, car_id, seller_id, city_id, deleted) VALUES ('2018-11-01', 'Auto je uvezen iz nemacke. Bez skrivene mane u odlicnom stanju', 150000, 2050, 2004, 2, 2, 5 , 0)
INSERT INTO car_add(date_created, details, mileage, price, year, car_id, seller_id, city_id, deleted) VALUES ('2018-11-01', 'Auto kupljen u srbiji. Redovno servisiran kod ovlascenog servisera. Zadnji branik je zamenjen zbog udarca', 150000, 2050, 2004, 3, 2, 6 , 0)

