package edmond.sf64.CarbuyerBackend.converters;

import edmond.sf64.CarbuyerBackend.Dtos.CarAddCreateDto;
import edmond.sf64.CarbuyerBackend.Entities.CarAdd;
import edmond.sf64.CarbuyerBackend.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AddCreateDtoToCarAdd implements Converter<CarAddCreateDto, CarAdd> {

    @Autowired
    CarMakeRepository carMakeRepository;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    SellerRepository sellerRepository;

    @Autowired
    CarModelRepository carModelRepository;

    @Override
    public CarAdd convert(CarAddCreateDto createDto) {
        CarAdd carAdd = new CarAdd();
        carAdd.setDeleted(false);
        carAdd.setCarModel(carModelRepository.getOne((long) createDto.getModel()));
        carAdd.setCity(cityRepository.getOne((long) createDto.getCity()));
        carAdd.setDateCreated(new Date());
        carAdd.setDetails(createDto.getDetails());
        carAdd.setMileage(createDto.getMileage());
        carAdd.setPrice(createDto.getPrice());
        carAdd.setYear(createDto.getYear());
        carAdd.setSeller(sellerRepository.getOne((long) createDto.getSeller()));

        return carAdd;

    }
}
