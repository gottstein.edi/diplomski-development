package edmond.sf64.CarbuyerBackend.Security;

public class UserTokenState {
    private String access_token;
    private Long expires_in;
    private String role;
    private Long id;

    public UserTokenState(String access_token, long expires_in, String role, long id) {
        this.access_token = access_token;
        this.expires_in = expires_in;
        this.role = role;
        this.id = id;
    }

    public UserTokenState(){
        this.access_token = null;
        this.expires_in = null;
        this.role = null;
        this.id = null;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
