package edmond.sf64.CarbuyerBackend.Repositories;

import edmond.sf64.CarbuyerBackend.Entities.CarAdd;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CarAddRepository extends JpaRepository<CarAdd, Long> {

    CarAdd findByDeletedAndId(boolean deleted, Long id);
    Page<CarAdd> findAllByDeleted(Pageable pageable, boolean deleted);
    Set<CarAdd> findAllBySeller_idAndDeleted(Long id,boolean deleted);

}
