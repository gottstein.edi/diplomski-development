package edmond.sf64.CarbuyerBackend.Repositories;

import edmond.sf64.CarbuyerBackend.Entities.CarMake;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarMakeRepository extends JpaRepository<CarMake, Long> {

    List<CarMake> findAllByDeleted(boolean deleted);
    CarMake findByDeletedAndId(boolean deleted, Long id);


}
