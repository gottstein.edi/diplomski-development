package edmond.sf64.CarbuyerBackend.Repositories;

import edmond.sf64.CarbuyerBackend.Entities.Seller;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerRepository extends JpaRepository<Seller, Long> {

    Page<Seller> findAllByUsernameIgnoreCaseContainsAndDeleted(String username, Pageable pageable, boolean deleted);
}
