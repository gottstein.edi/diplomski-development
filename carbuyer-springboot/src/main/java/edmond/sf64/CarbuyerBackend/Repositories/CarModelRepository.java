package edmond.sf64.CarbuyerBackend.Repositories;

import edmond.sf64.CarbuyerBackend.Entities.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarModelRepository extends JpaRepository<CarModel, Long> {
    List<CarModel> findAllByDeleted(boolean deleted);
    CarModel findAllByDeletedAndId(boolean deleted, Long id);
    List<CarModel> findAllByMake_idAndDeleted(Long id, boolean deleted);

}
