package edmond.sf64.CarbuyerBackend.Repositories;

import edmond.sf64.CarbuyerBackend.Entities.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    Page<City> findAllByNameIgnoreCaseContainsAndDeleted(String name,Pageable pageable, boolean deleted);
}
