package edmond.sf64.CarbuyerBackend.Repositories;

import edmond.sf64.CarbuyerBackend.Entities.Seller;
import edmond.sf64.CarbuyerBackend.Entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsernameAndDeleted(String username, boolean deleted);
    User findByDeletedAndId(boolean deleted, Long id);
    Page<User> findAllByUsernameIgnoreCaseContainsAndDeleted(String username, Pageable pageable, boolean deleted);
    boolean existsByUsername(String username);
}
