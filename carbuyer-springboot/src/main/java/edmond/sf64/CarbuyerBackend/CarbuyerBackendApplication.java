package edmond.sf64.CarbuyerBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarbuyerBackendApplication {


	public static void main(String[] args) {
		SpringApplication.run(CarbuyerBackendApplication.class, args);
	}
}
