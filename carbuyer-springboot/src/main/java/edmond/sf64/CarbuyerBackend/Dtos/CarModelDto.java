package edmond.sf64.CarbuyerBackend.Dtos;

import edmond.sf64.CarbuyerBackend.Entities.CarModel;

public class CarModelDto {

    private long id;
    private String name;
    private long carMake;

    public CarModelDto() {
    }

    public CarModelDto(CarModel carModel) {
        this.id = carModel.getId();
        this.name = carModel.getName();
        this.carMake = carModel.getMake().getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCarMake() {
        return carMake;
    }

    public void setCarMake(long carMake) {
        this.carMake = carMake;
    }
}
