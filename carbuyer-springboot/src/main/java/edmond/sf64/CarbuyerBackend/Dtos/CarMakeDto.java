package edmond.sf64.CarbuyerBackend.Dtos;

import edmond.sf64.CarbuyerBackend.Entities.CarMake;

public class CarMakeDto {

    private long id;
    private String name;

    public CarMakeDto(CarMake carMake) {
        this.id = carMake.getId();
        this.name = carMake.getName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
