package edmond.sf64.CarbuyerBackend.Dtos;

import edmond.sf64.CarbuyerBackend.Entities.Seller;

public class SellerDto {

    private long id;
    private String firstName;
    private String lastName;
    private String username;
    private String address;
    private String phoneNumber;
    private String email;
    private String role;

    public SellerDto(Seller seller){
        this.id = seller.getId();
        this.firstName = seller.getFirstName();
        this.lastName = seller.getLastName();
        this.username = seller.getUsername();
        this.address = seller.getAddress();
        this.phoneNumber = seller.getPhoneNumber();
        this.email = seller.getEmail();
        this.role = seller.getRole().getName();
    }

    public SellerDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
