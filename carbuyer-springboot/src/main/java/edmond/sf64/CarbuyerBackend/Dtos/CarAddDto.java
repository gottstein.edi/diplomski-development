package edmond.sf64.CarbuyerBackend.Dtos;

import edmond.sf64.CarbuyerBackend.Entities.CarAdd;

import java.util.Date;

public class CarAddDto {

    private Long id;
    private String details;
    private Integer price;
    private Date dateCreated;
    private Integer mileage;
    private Integer year;
    private Long seller;
    private String make;
    private String model;
    private String firstImage;
    private String city;

    public CarAddDto(CarAdd carAdd) {
        this.id = carAdd.getId();
        this.details = carAdd.getDetails();
        this.price = carAdd.getPrice();
        this.dateCreated = carAdd.getDateCreated();
        this.mileage = carAdd.getMileage();
        this.year = carAdd.getYear();
        this.city = carAdd.getCity().getName();
        if(carAdd.getImages().size() != 0){
            this.firstImage = carAdd.getImages().iterator().next().getUrl();
        } else {
            this.firstImage = null;
        }


        this.seller = carAdd.getSeller().getId();
        this.make = carAdd.getCarModel().getMake().getName();
        this.model = carAdd.getCarModel().getName();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getSeller() {
        return seller;
    }

    public void setSeller(Long seller) {
        this.seller = seller;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFirstImage() {
        return firstImage;
    }

    public void setFirstImage(String firstImage) {
        this.firstImage = firstImage;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
