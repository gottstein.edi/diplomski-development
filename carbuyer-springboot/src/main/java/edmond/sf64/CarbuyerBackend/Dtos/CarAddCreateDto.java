package edmond.sf64.CarbuyerBackend.Dtos;

import edmond.sf64.CarbuyerBackend.Entities.CarAdd;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CarAddCreateDto {

    @NotNull
    @Min(1)
    private Integer city;
    @NotNull
    private String details;
    @NotNull
    @Min(1)
    private Integer make;
    @NotNull
    @Min(0)
    private Integer mileage;
    @NotNull
    @Min(1)
    private Integer model;
    @NotNull
    @Min(0)
    private Integer price;
    @NotNull
    @Min(0)
    private Integer seller;
    @NotNull
    @Min(1885)
    private Integer year;


    public CarAddCreateDto() {
    }



    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getMake() {
        return make;
    }

    public void setMake(Integer make) {
        this.make = make;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Integer getModel() {
        return model;
    }

    public void setModel(Integer model) {
        this.model = model;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSeller() {
        return seller;
    }

    public void setSeller(Integer seller) {
        this.seller = seller;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
