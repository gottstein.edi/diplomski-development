package edmond.sf64.CarbuyerBackend.Dtos;

import edmond.sf64.CarbuyerBackend.Entities.Image;

public class ImageDto {

    private long id;
    private String url;
    private String imageKey;
    private String name;
    private long add;

    public ImageDto() {
    }

    public ImageDto(Image image){
        this.url = image.getUrl();
        this.imageKey = image.getImageKey();
        this.name = image.getName();
        this.add = image.getAdd().getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageKey() {
        return imageKey;
    }

    public void setImageKey(String imageKey) {
        this.imageKey = imageKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAdd() {
        return add;
    }

    public void setAdd(long add) {
        this.add = add;
    }
}
