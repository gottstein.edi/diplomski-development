package edmond.sf64.CarbuyerBackend.Entities;

import edmond.sf64.CarbuyerBackend.Dtos.ImageDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String url;

    @NotNull
    private String name;

    @NotNull
    private String imageKey;

    @ManyToOne
    @JoinColumn(name = "add_id")
    private CarAdd add;

    public Image() {
    }

    public Image(ImageDto dto){
        this.url = dto.getUrl();
        this.name = dto.getName();
        this.imageKey = dto.getImageKey();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageKey() {
        return imageKey;
    }

    public void setImageKey(String imageKey) {
        this.imageKey = imageKey;
    }

    public CarAdd getAdd() {
        return add;
    }

    public void setAdd(CarAdd add) {
        this.add = add;
    }
}
