package edmond.sf64.CarbuyerBackend.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Seller extends User {

    @NotNull
    @Size(min = 1, max = 50)
    private String email;

    @NotNull
    @Size(min = 1, max = 50)
    private String address;

    @NotNull
    @Size(min = 1, max = 20)
    private String phoneNumber;

    @OneToMany(mappedBy = "seller", cascade = CascadeType.ALL)
    private Set<CarAdd> adds = new HashSet<>();

    public Seller() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Set<CarAdd> getAdds() {
        return adds;
    }

    public void setAdds(Set<CarAdd> adds) {
        this.adds = adds;
    }


}
