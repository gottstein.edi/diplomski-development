package edmond.sf64.CarbuyerBackend.Services.impl;

import edmond.sf64.CarbuyerBackend.Dtos.CarMakeDto;
import edmond.sf64.CarbuyerBackend.Entities.CarMake;
import edmond.sf64.CarbuyerBackend.Entities.CarModel;
import org.springframework.beans.factory.annotation.Autowired;
import edmond.sf64.CarbuyerBackend.Repositories.CarMakeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarMakeService {

    @Autowired
    CarMakeRepository carMakeRepository;

    public List<CarMake> findAll() {
        return carMakeRepository.findAllByDeleted(false);
    }

    public CarMake findOne(Long id){
        return carMakeRepository.findByDeletedAndId(false, id);
    }

    public Boolean delete(Long id) {
        CarMake carMake = carMakeRepository.getOne(id);
        carMake.setDeleted(true);
        carMakeRepository.saveAndFlush(carMake);
        return true;
    }

    public CarMake save(CarMake make) {
        return carMakeRepository.save(make);
    }


}
