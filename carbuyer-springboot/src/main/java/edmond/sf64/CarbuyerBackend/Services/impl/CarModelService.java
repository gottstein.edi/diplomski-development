package edmond.sf64.CarbuyerBackend.Services.impl;

import edmond.sf64.CarbuyerBackend.Entities.CarMake;
import edmond.sf64.CarbuyerBackend.Entities.CarModel;
import edmond.sf64.CarbuyerBackend.Repositories.CarModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarModelService {

    @Autowired
    CarModelRepository carModelRepository;

    public Boolean delete(Long id) {
        CarModel carModel = carModelRepository.getOne(id);
        carModel.setDeleted(true);
        carModelRepository.saveAndFlush(carModel);
        return true;
    }

    public List<CarModel> findAllByMake (Long makeId) {
        return carModelRepository.findAllByMake_idAndDeleted(makeId, false);
    }

    public CarModel save(CarModel model){
        return carModelRepository.save(model);
    }


}
