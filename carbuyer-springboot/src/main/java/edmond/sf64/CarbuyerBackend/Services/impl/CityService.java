package edmond.sf64.CarbuyerBackend.Services.impl;

import edmond.sf64.CarbuyerBackend.Entities.City;
import edmond.sf64.CarbuyerBackend.Repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {

    @Autowired
    CityRepository cityRepo;

    public Page<City> getAll(String name, Pageable pageable){
        return cityRepo.findAllByNameIgnoreCaseContainsAndDeleted(name, pageable, false);
    }

    public City findOne(Long id) {
        return cityRepo.getOne(id);
    }

    public City save(City city){
        return cityRepo.save(city);
    }

    public Boolean delete(Long id){
        City city = cityRepo.getOne(id);
        city.setDeleted(true);
        cityRepo.saveAndFlush(city);
        return true;
    }
}
