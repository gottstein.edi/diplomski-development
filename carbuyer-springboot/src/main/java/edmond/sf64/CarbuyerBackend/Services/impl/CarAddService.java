package edmond.sf64.CarbuyerBackend.Services.impl;

import edmond.sf64.CarbuyerBackend.Entities.CarAdd;
import edmond.sf64.CarbuyerBackend.Entities.Image;
import edmond.sf64.CarbuyerBackend.Repositories.CarAddRepository;
import edmond.sf64.CarbuyerBackend.Repositories.ImageRepository;
import edmond.sf64.CarbuyerBackend.Services.IAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarAddService implements IAdService {

    @Autowired
    CarAddRepository carAddRepository;

    @Autowired
    ImageRepository imageRepository;

    public Page<CarAdd> findAll(Pageable pageable){
        return carAddRepository.findAllByDeleted(pageable, false);
    }

    public CarAdd findOne(Long id) {
        return carAddRepository.findByDeletedAndId(false, id);
    }

    public Boolean delete(Long id){
        CarAdd carAdd = carAddRepository.getOne(id);
        carAdd.setDeleted(true);
        carAddRepository.saveAndFlush(carAdd);
        for(Image i: carAdd.getImages()){
            imageRepository.delete(i);
        }
        return true;
    }
}
