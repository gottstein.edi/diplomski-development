package edmond.sf64.CarbuyerBackend.Services.impl;

import edmond.sf64.CarbuyerBackend.Dtos.PasswordDto;
import edmond.sf64.CarbuyerBackend.Entities.User;
import edmond.sf64.CarbuyerBackend.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepo;

    public Page<User> findAll(String username, Pageable pageable) {
        return userRepo.findAllByUsernameIgnoreCaseContainsAndDeleted(username, pageable, false);
    }

    public User findOne(Long id){
        return userRepo.findByDeletedAndId(false, id);
    }

    public Boolean delete(Long id) {
        User user = userRepo.findByDeletedAndId(false, id);
        user.setDeleted(true);
        userRepo.saveAndFlush(user);
        return true;
    }

    public ResponseEntity updatePassowrd(User user, PasswordDto passwordDto){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if(!(bCryptPasswordEncoder.matches(passwordDto.getOldPassword(), user.getPassword())))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        user.setPassword(bCryptPasswordEncoder.encode(passwordDto.getNewPassword()));
        userRepo.save(user);
        return new ResponseEntity(HttpStatus.OK);
    }
}
