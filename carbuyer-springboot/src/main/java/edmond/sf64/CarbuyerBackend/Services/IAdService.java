package edmond.sf64.CarbuyerBackend.Services;

import edmond.sf64.CarbuyerBackend.Entities.CarAdd;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IAdService {

    Page<CarAdd> findAll(Pageable pageable);
    CarAdd findOne(Long id);
    Boolean delete(Long id);
}
