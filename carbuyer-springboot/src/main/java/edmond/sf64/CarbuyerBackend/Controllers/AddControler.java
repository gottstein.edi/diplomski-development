package edmond.sf64.CarbuyerBackend.Controllers;

import edmond.sf64.CarbuyerBackend.Dtos.CarAddCreateDto;
import edmond.sf64.CarbuyerBackend.Dtos.CarAddDto;
import edmond.sf64.CarbuyerBackend.Dtos.ImageDto;
import edmond.sf64.CarbuyerBackend.Entities.CarAdd;
import edmond.sf64.CarbuyerBackend.Entities.Image;
import edmond.sf64.CarbuyerBackend.Repositories.CarAddRepository;
import edmond.sf64.CarbuyerBackend.Repositories.ImageRepository;
import edmond.sf64.CarbuyerBackend.Services.IAdService;
import edmond.sf64.CarbuyerBackend.Services.impl.CarAddService;
import edmond.sf64.CarbuyerBackend.Services.impl.CityService;
import edmond.sf64.CarbuyerBackend.converters.AddCreateDtoToCarAdd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/adds")
public class AddControler {

    @Autowired
    IAdService carAddService;

    @Autowired
    CarAddRepository carAddRepository;

    @Autowired
    AddCreateDtoToCarAdd toCarAdd;

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    CityService cityService;

    @GetMapping
    public ResponseEntity getAll(Pageable pageable){
        Page<CarAdd> adds = carAddService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Total", String.valueOf(adds.getTotalElements()));

        return ResponseEntity.ok().headers(headers).body(adds.getContent().stream().map(CarAddDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id){
        CarAdd carAdd = carAddService.findOne(id);
        if(carAdd == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        CarAddDto dto = new CarAddDto(carAddService.findOne(id));
        return ResponseEntity.ok().body(dto);
    }



    @GetMapping("/{id}/images")
    public ResponseEntity getAddImages(@PathVariable("id") long id){
        CarAdd carAdd = carAddService.findOne(id);
        if(carAdd == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(carAdd.getImages().stream().map(ImageDto::new).collect(Collectors.toList()));
    }

    @PostMapping
    @PreAuthorize("hasRole('SELLER')")
    public ResponseEntity createOne(@RequestBody @Validated CarAddCreateDto createDto, Errors errors){
        if(createDto == null || errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok().body(new CarAddDto(carAddRepository.save(toCarAdd.convert(createDto))));

    }

    @PostMapping("/{id}/images")
    @PreAuthorize("hasRole('SELLER')")
    public ResponseEntity uploadImages(@PathVariable("id") long id, @RequestBody List<ImageDto> imageDtoList){
        CarAdd carAdd = carAddService.findOne(id);
        if(carAdd == null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        for (ImageDto img: imageDtoList){
            Image image = new Image(img);
            image.setAdd(carAdd);
            imageRepository.save(image);
        }
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('SELLER','ADMIN')")
    public ResponseEntity deleteAdd(@PathVariable("id") long id){
        CarAdd carAdd = carAddService.findOne(id);
        if(carAdd == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        carAddService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
