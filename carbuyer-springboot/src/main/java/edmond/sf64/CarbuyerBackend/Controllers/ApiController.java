package edmond.sf64.CarbuyerBackend.Controllers;


import edmond.sf64.CarbuyerBackend.Dtos.PasswordDto;
import edmond.sf64.CarbuyerBackend.Dtos.SellerDto;
import edmond.sf64.CarbuyerBackend.Dtos.UserDto;
import edmond.sf64.CarbuyerBackend.Entities.Seller;
import edmond.sf64.CarbuyerBackend.Entities.User;
import edmond.sf64.CarbuyerBackend.Repositories.UserRepository;
import edmond.sf64.CarbuyerBackend.Services.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @GetMapping("/me")
    public ResponseEntity getMe(Principal principal) {
        User user = userRepository.findByUsernameAndDeleted(principal.getName(), false);
        if(user instanceof Seller){
            return ResponseEntity.ok(new SellerDto((Seller) user));
        }
        return ResponseEntity.ok(new UserDto(user));
    }

    @PutMapping("/me/change-password")
    @PreAuthorize("hasAnyRole('SELLER','ADMIN')")
    public ResponseEntity changePassword(Principal principal, @RequestBody PasswordDto passwordDto, Errors errors){
        User user = userRepository.findByUsernameAndDeleted(principal.getName(), false);

        if(user == null){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        return userService.updatePassowrd(user, passwordDto);
    }


}
