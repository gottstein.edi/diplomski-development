package edmond.sf64.CarbuyerBackend.Controllers;

import edmond.sf64.CarbuyerBackend.Entities.Seller;
import edmond.sf64.CarbuyerBackend.Repositories.AuthorityRepository;
import edmond.sf64.CarbuyerBackend.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/register")
public class RegistrationController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    PasswordEncoder encoder;


    @PostMapping
    @PreAuthorize("hasRole('VISITOR')")
    public ResponseEntity register(@RequestBody Seller seller){

        if(seller == null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        seller.setRole(authorityRepository.getOne((long)2));
        seller.setPassword(encoder.encode(seller.getPassword()));

        userRepository.save(seller);
        return new ResponseEntity(HttpStatus.CREATED);

    }


}
