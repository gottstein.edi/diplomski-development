package edmond.sf64.CarbuyerBackend.Controllers;

import edmond.sf64.CarbuyerBackend.Entities.CarModel;
import edmond.sf64.CarbuyerBackend.Repositories.CarModelRepository;
import edmond.sf64.CarbuyerBackend.Services.impl.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cars")
public class CarModelController {

    @Autowired
    CarModelRepository carModelRepository;

    @Autowired
    CarModelService carModelService;

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteOne(@PathVariable("id") long id){
        CarModel carModel = carModelRepository.getOne(id);
        if(carModel == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        carModelService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
