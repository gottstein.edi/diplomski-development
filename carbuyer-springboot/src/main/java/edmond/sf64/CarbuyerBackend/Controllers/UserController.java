package edmond.sf64.CarbuyerBackend.Controllers;

import edmond.sf64.CarbuyerBackend.Dtos.CarAddDto;
import edmond.sf64.CarbuyerBackend.Dtos.SellerDto;
import edmond.sf64.CarbuyerBackend.Dtos.UserDto;
import edmond.sf64.CarbuyerBackend.Entities.Seller;
import edmond.sf64.CarbuyerBackend.Entities.User;
import edmond.sf64.CarbuyerBackend.Repositories.CarAddRepository;
import edmond.sf64.CarbuyerBackend.Repositories.UserRepository;
import edmond.sf64.CarbuyerBackend.Services.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CarAddRepository carAddRepository;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(@RequestParam(value = "username", defaultValue = "") String username, Pageable pageable){
        Page<User> users = userService.findAll(username, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Total", String.valueOf(users.getTotalElements()));

        return ResponseEntity.ok().headers(headers).body(users.getContent().stream().map(UserDto::new).collect(Collectors.toList()));

    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id){
        User user = userService.findOne(id);
        if(user == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if(user instanceof Seller){
            return ResponseEntity.ok().body(new SellerDto((Seller) user));
        }
        return ResponseEntity.ok().body(new UserDto(user));
    }

    @GetMapping("/{id}/adds")
    @PreAuthorize("hasAnyRole('SELLER','ADMIN')")
    public ResponseEntity getUserAdds(@PathVariable("id") long id){
        User user = userService.findOne(id);
        if(user == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (user instanceof Seller){
            return ResponseEntity.ok().body(carAddRepository.findAllBySeller_idAndDeleted(id, false).stream().map(CarAddDto::new).collect(Collectors.toList()));
        }

        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/username-check/{username}")
    public ResponseEntity checkUsernameAvailability(@PathVariable("username") String username){
        if(username == null){
            return ResponseEntity.badRequest().body("");
        }
        return ResponseEntity.ok().body(!userRepository.existsByUsername(username));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteOne(@PathVariable("id") long id){
        User user = userService.findOne(id);
        if(user == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        userService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}