package edmond.sf64.CarbuyerBackend.Controllers;

import edmond.sf64.CarbuyerBackend.Dtos.CarMakeDto;
import edmond.sf64.CarbuyerBackend.Dtos.CarModelDto;
import edmond.sf64.CarbuyerBackend.Entities.CarMake;
import edmond.sf64.CarbuyerBackend.Entities.CarModel;
import edmond.sf64.CarbuyerBackend.Services.impl.CarMakeService;
import edmond.sf64.CarbuyerBackend.Services.impl.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/makes")
public class CarMakeController {

    @Autowired
    CarMakeService carMakeService;

    @Autowired
    CarModelService carModelService;


    @GetMapping
    public ResponseEntity getAll(){
        List<CarMake> carMakes = carMakeService.findAll();
        if(carMakes == null || carMakes.size() == 0){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        return ResponseEntity.ok().body(carMakes.stream().map(CarMakeDto::new).collect(Collectors.toList()));
    }

//    Get car models by make
    @GetMapping("/{id}/cars")
    public ResponseEntity getMakeCars(@PathVariable("id") long id){
        CarMake carMake = carMakeService.findOne(id);
        if(carMake == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(carModelService.findAllByMake(id).stream().map(CarModelDto::new).collect(Collectors.toList()));
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity postOne(@Validated @RequestBody CarMake make, Errors errors){

        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(make != null){
            carMakeService.save(make);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{id}/cars")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity addCarsToMake(@PathVariable("id") long id, @Validated @RequestBody CarModel model, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        CarMake carMake = carMakeService.findOne(id);
        if(carMake == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        model.setMake(carMake);
        carModelService.save(model);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteOne(@PathVariable("id") long id){
        CarMake carMake = carMakeService.findOne(id);
        if(carMake == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        carMakeService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }




}
