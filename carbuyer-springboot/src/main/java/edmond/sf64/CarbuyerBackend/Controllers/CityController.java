package edmond.sf64.CarbuyerBackend.Controllers;

import edmond.sf64.CarbuyerBackend.Entities.City;
import edmond.sf64.CarbuyerBackend.Services.impl.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cities")
public class CityController {

    @Autowired
    CityService cityService;

    @GetMapping
    public ResponseEntity getAllCities(@RequestParam(value = "name", defaultValue = "") String name, Pageable pageable){
        Page<City> cities = cityService.getAll(name, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Total", String.valueOf(cities.getTotalElements()));

        return ResponseEntity.ok().headers(headers).body(cities.getContent());

    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id){
        City city = cityService.findOne(id);
        if(city == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(city);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity postOne(@Validated @RequestBody City city, Errors errors){

        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(city != null){
            cityService.save(city);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity updateOne(@PathVariable("id") long id, @Validated @RequestBody City city, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(city != null){
            if(city.getId() == id && city.getId() != 0){
                cityService.save(city);
                return new ResponseEntity(HttpStatus.OK);
            }
        }

        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteOne(@PathVariable("id") long id){
        City city = cityService.findOne(id);
        if(city == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        cityService.delete(city.getId());
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
